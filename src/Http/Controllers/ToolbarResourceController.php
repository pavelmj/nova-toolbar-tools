<?php

namespace Gabelbart\Laravel\Nova\ToolbarTools\Http\Controllers;

use Gabelbart\Laravel\Nova\ToolbarTools\Http\Requests\ToolbarResourceRequest;
use Gabelbart\Laravel\Nova\ToolbarTools\Tools\ToolbarResource;

class ToolbarResourceController
{
    public function get(ToolbarResourceRequest $request)
    {
        /** @var ToolbarResource $tool */
        $tool = $request->getMatchingTools()->first();
        return $tool->handleIndexRequest($request);
    }

    public function post(ToolbarResourceRequest $request)
    {
        $request->getMatchingTools()
            ->map(fn (ToolbarResource $tool) => $tool->handlePutRequest($request));
    }
}
