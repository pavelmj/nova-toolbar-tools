<?php

namespace Gabelbart\Laravel\Nova\ToolbarTools\Tools;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Session;

use Laravel\Nova\GlobalSearch;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Nova;
use Laravel\Nova\Resource;

class ToolbarResource extends ToolbarTool
{
    public string $component = 'toolbar-resource';
    protected $resourceClass;
    protected $changeCallback = null;
    protected $indexCallback = null;
    protected bool $saveToSession = true;

    /**
     * @throws \Exception
     */
    public function __construct($resourceClass)
    {
        if (
            is_callable([$resourceClass, 'newResource']) &&
            $resourceClass::newResource() instanceof Resource
        ) {
            throw new \Exception('Nova class missing!!!');
        }

        $this->resourceClass = $resourceClass;
        $this->selection($this->getSelectionFromSession());
        $this->withLabel($this->resourceClass::singularLabel());
    }

    public function saveToSession(bool $flag): self
    {
        $this->saveToSession = $flag;

        return $this;
    }
    public function selection($value): self
    {
        $model = ($value instanceof Model) ? $value : ($this->resourceClass::$model)::find($value);

        if ($model) {
            $request = app(NovaRequest::class);
            $resource = new $this->resourceClass($model);
            $this->withMeta([
                'selection' => [
                    'id' => $model->getKey(),
                    'title' => $resource->title(),
                    'subTitle' => transform($resource->subtitle(), function ($subtitle) {
                        return (string) $subtitle;
                    }),
                    'avatar' => $resource->resolveAvatarUrl($request),
                    'rounded' => $resource->resolveIfAvatarShouldBeRounded($request),
                ]
            ]);
        }

        return $this;
    }
    public function searchable(bool $value = true): self
    {
        $this->withMeta([
            'searchable' => $value
        ]);

        return $this;
    }
    public function withSearchHotkey(string $value, ?string $label = null): self
    {
        $this->withMeta([
            'shortcut' => $value,
            'shortcutLabel' => $label ? $label : $value
        ]);

        return $this;
    }
    public function withLabel(string $value): self
    {
        $this->withMeta([
            'label' => $value
        ]);
        return $this;
    }
    public function onChange($callback): self
    {
        if (is_callable($callback)) {
            $this->changeCallback = $callback;
        }

        return $this;
    }
    public function withIndexQuery($callback): self
    {
        if (is_callable($callback)) {
            $this->indexCallback = $callback;
        }

        return $this;
    }

    public function isForModel($modelClass): bool
    {
        return (is_string($modelClass) ? app($modelClass) : $modelClass) instanceof $this->resourceClass::$model;
    }

    protected function sessionKey(): string
    {
        $resourceRouteKey = $this->resourceClass::uriKey();
        return "nova.toolbar-resources.$resourceRouteKey";
    }

    protected function getValueFromSession(): ?string
    {
        return Session::get($this->sessionKey(), null);
    }

    protected function getSelectionFromSession(): ?Model
    {
        return  ($this->resourceClass::$model)::find($this->getValueFromSession());
    }

    protected function writeSelectionToSession($value)
    {
        Session::put($this->sessionKey(), $value);
    }

    public function handlePutRequest(NovaRequest $request): void
    {
        if ($this->saveToSession) {
            $this->writeSelectionToSession($request->get('id', null));
        }

        if (!is_null($this->changeCallback)) {
            ($this->changeCallback)($request);
        }
    }

    /**
     * @param NovaRequest $request
     * @return array|JsonResponse
     */
    public function handleIndexRequest(NovaRequest $request)
    {
        if ($request->has('search') &&
            strlen($request->get('search')) > 0
        ) {
            return (new GlobalSearch(
                $request, [$this->resourceClass]
            ))->get();
        } else {
            /** @var Builder $query */
            $query = ($this->resourceClass::$model)::query();
            if (is_callable($this->indexCallback)) {
                $result = ($this->indexCallback)($query);
                if ($result instanceof Builder) {
                    $query = $result;
                }
            }

            /** @var Collection $models */
            $models = $query->get();
            $list = $models->map(function (Model $model) use ($request) {
                /** @var Resource $resource */
                $resource = (new $this->resourceClass($model));

                return [
                    'id' => $model->getKey(),
                    'title' => $resource->title(),
                    'subTitle' => transform($resource->subtitle(), function ($subtitle) {
                        return (string) $subtitle;
                    }),
                    'avatar' => $resource->resolveAvatarUrl($request),
                    'rounded' => $resource->resolveIfAvatarShouldBeRounded($request),
                ];
            });

            return response()->json($list);
        }
    }

    public function jsonSerialize()
    {
        return array_merge(
            parent::jsonSerialize(),
            [
                'resource' => $this->resourceClass::uriKey()
            ]
        );
    }

    /**
     * @throws \Exception
     */
    public static function sessionValueFor($resourceClass)
    {
        return (new self($resourceClass))->getSelectionFromSession();
    }

    /**
     * @throws \Exception
     */
    public static function rawSessionValueFor($resourceClass)
    {
        return (new self($resourceClass))->getValueFromSession();
    }
}
