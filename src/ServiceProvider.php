<?php

namespace Gabelbart\Laravel\Nova\ToolbarTools;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

use Laravel\Nova\Events\ServingNova;
use Laravel\Nova\Nova;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(ToolbarToolsService::class, fn () => new ToolbarToolsService());
        $this->app->booted(function () {
            $this->routes();
        });
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'toolbar-tools');

        Nova::serving(function (ServingNova $event) {
            Nova::script('toolbar-tools', __DIR__ . '/../dist/js/main.js');
            Nova::style('toolbar-tools', __DIR__ . '/../dist/css/main.css');
        });
    }

    /**
     * Register the tool's routes.
     *
     * @return void
     */
    protected function routes()
    {
        if ($this->app->routesAreCached()) {
            return;
        }

        Route::middleware(['nova'])
            ->prefix('nova-vendor/nova-toolbar-tools')
            ->group(__DIR__.'/../routes/api.php');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
