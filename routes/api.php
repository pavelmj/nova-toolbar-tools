<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use Gabelbart\Laravel\Nova\ToolbarTools\Http\Middleware\AuthorizeToolbarResource;

/*
|--------------------------------------------------------------------------
| Tool API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your tool. These routes
| are loaded by the ServiceProvider of your tool. They are protected
| by your tool's "Authorize" middleware by default. Now, go build!
|
*/

Route::prefix('tools')
    ->group(function () {
        Route::get('', [ \Gabelbart\Laravel\Nova\ToolbarTools\Http\Controllers\ToolbarToolsController::class, 'get' ]);
    });

Route::middleware([AuthorizeToolbarResource::class])
    ->prefix('toolbar-resources')
    ->group(__DIR__.'/toolbar-resources.php');
